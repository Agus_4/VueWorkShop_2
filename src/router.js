import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Tablas from './views/Tablas.vue';
import Charts from './views/Charts.vue';
import Drag from './views/DragAndDrop.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/tablas',
      name: 'tablas',
      component: Tablas,
    },
    {
      path: '/charts',
      name: 'charts',
      component: Charts,
    },
    {
      path: '/drag',
      name: 'drag',
      component: Drag,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component() {
        return import('./views/About.vue');
      },
    },
    {
      path: '/form',
      name: 'form',
      component() {
        return import('./views/Form.vue');
      },
    },
  ],
});
